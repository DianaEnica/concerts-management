DO $body$
DECLARE

BEGIN
	
	CREATE SEQUENCE seq_gen_user
	  INCREMENT 1
	  MINVALUE 1
	  MAXVALUE 9223372036854775807
	  START 1
	  CACHE 1;

	CREATE SEQUENCE seq_gen_role
	  INCREMENT 1
	  MINVALUE 1
	  MAXVALUE 9223372036854775807
	  START 1
	  CACHE 1;

	CREATE SEQUENCE seq_gen_user_role
	  INCREMENT 1
	  MINVALUE 1
	  MAXVALUE 9223372036854775807
	  START 1
	  CACHE 1;

	CREATE SEQUENCE seq_gen_concert
	  INCREMENT 1
	  MINVALUE 1
	  MAXVALUE 9223372036854775807
	  START 1
	  CACHE 1;

	CREATE SEQUENCE seq_gen_ticket
	  INCREMENT 1
	  MINVALUE 1
	  MAXVALUE 9223372036854775807
	  START 1
	  CACHE 1;
	  
	CREATE TABLE users (
  	  id           bigint                      NOT NULL,
   	  email        character varying(60) 	   NOT NULL,
  	  username     character varying(60)	   NOT NULL,
	  password     character varying(60) 	   NOT NULL,
 	  created_at   timestamp without time zone NOT NULL
	);

	CREATE TABLE roles (
  	  id            bigint                      NOT NULL,
   	  role_name     character varying(60)       NOT NULL,
  	  role_code     character varying(60)       NOT NULL
	);

	CREATE TABLE user_roles (
	  id            bigint                      NOT NULL,
	  user_id	    bigint                      NOT NULL,
	  role_id	    bigint               	    NOT NULL
	);

	CREATE TABLE concerts (
	  id            bigint                	    NOT NULL,
	  name		    character varying(60) 	    NOT NULL,
	  id_organiser  bigint               	    NOT NULL,
	  concert_date  timestamp without time zone NOT NULL
	);

	CREATE TABLE tickets (
	  id               bigint             	        NOT NULL,
	  concert_id       bigint                       NOT NULL,	
	  user_id          bigint              	        NOT NULL,	
	  price 	       decimal                      NOT NULL,
	  purchased_date   timestamp without time zone  NOT NULL
	);

	
	ALTER TABLE users 
	ADD CONSTRAINT users_id_pk PRIMARY KEY(id);

	ALTER TABLE roles 
	ADD CONSTRAINT roles_id_pk PRIMARY KEY(id);

	ALTER TABLE user_roles 
	ADD CONSTRAINT user_roles_id_pk PRIMARY KEY(id);

	ALTER TABLE concerts 
	ADD CONSTRAINT concerts_id_pk PRIMARY KEY(id);

	ALTER TABLE tickets 
	ADD CONSTRAINT tickets_id_pk PRIMARY KEY(id);


	ALTER TABLE user_roles 
	ADD CONSTRAINT user_roles_users_fk FOREIGN KEY (user_id) REFERENCES users(id) MATCH SIMPLE ON DELETE NO ACTION ON UPDATE NO ACTION;

	ALTER TABLE user_roles 
	ADD CONSTRAINT user_roles_roles_fk FOREIGN KEY (role_id) REFERENCES roles(id) MATCH SIMPLE ON DELETE NO ACTION ON UPDATE NO ACTION;

	ALTER TABLE concerts 
	ADD CONSTRAINT concerts_users_fk FOREIGN KEY (id_organiser) REFERENCES users(id) MATCH SIMPLE ON DELETE NO ACTION ON UPDATE NO ACTION;

	ALTER TABLE tickets 
	ADD CONSTRAINT tickets_concerts_fk FOREIGN KEY (concert_id) REFERENCES concerts(id) MATCH SIMPLE ON DELETE NO ACTION ON UPDATE NO ACTION;

	ALTER TABLE tickets 
	ADD CONSTRAINT tickets_users_fk FOREIGN KEY (user_id) REFERENCES users(id) MATCH SIMPLE ON DELETE NO ACTION ON UPDATE NO ACTION;


	INSERT INTO roles (id, role_name, role_code)
    VALUES (1, 'Organiser', 0);

    INSERT INTO roles (id, role_name, role_code)
    VALUES (2, 'Admin', 1);

    INSERT INTO roles (id, role_name, role_code)
    VALUES (3, 'Spectator', 2);
	
END
$body$ language plpgsql;
