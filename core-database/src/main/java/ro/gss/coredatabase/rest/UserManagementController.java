package ro.gss.coredatabase.rest;

import java.util.Collection;
import java.util.Objects;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ro.gss.coredatabase.dto.UserDto;
import ro.gss.coredatabase.entity.User;
import ro.gss.coredatabase.service.UserManagementService;
import ro.gss.coredatabase.validation.UserValidator;

@RestController
@RequestMapping("/api")
public class UserManagementController {

	private final UserManagementService userManagementService;
	private final UserValidator userValidator;

	@Autowired
	public UserManagementController(UserManagementService userManagementService, UserValidator userValidator) {
		this.userManagementService = Objects.requireNonNull(userManagementService,
				"userManagementService must be non null");
		this.userValidator = Objects.requireNonNull(userValidator, "userValidator must be non null");
	}

	@GetMapping("/users")
	public Collection<User> getAllUsers() {
		return userManagementService.getAllUsers();
	}

	@PostMapping("/users/add")
	public void addUser(@RequestBody @Valid UserDto userdto, BindingResult result) {
		userValidator.validate(userdto, result);
		if (result.hasErrors()) {
			System.err.println(result.getAllErrors());
		} else {
			userManagementService.addUser(userdto);
		}
	}
}
