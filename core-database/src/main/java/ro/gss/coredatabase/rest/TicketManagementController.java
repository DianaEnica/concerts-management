package ro.gss.coredatabase.rest;

import java.util.Collection;
import java.util.Objects;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ro.gss.coredatabase.dto.TicketDto;
import ro.gss.coredatabase.entity.Ticket;
import ro.gss.coredatabase.service.TicketManagementService;
import ro.gss.coredatabase.validation.TicketValidator;
import ro.gss.coredatabase.validation.UserTicketValidator;

@RestController
@RequestMapping("/api")
public class TicketManagementController {

	private final TicketManagementService ticketManagementService;
	private final TicketValidator ticketValidator;
	private final UserTicketValidator userTicketValidator;

	@Autowired
	public TicketManagementController(TicketManagementService ticketManagementService,
			final TicketValidator ticketValidator, final UserTicketValidator userTicketValidator) {
		
		this.ticketManagementService = Objects.requireNonNull(ticketManagementService,
				"ticketManagementService must be non null");
		this.ticketValidator = Objects.requireNonNull(ticketValidator, "ticketValidator must be non null");
		this.userTicketValidator = Objects.requireNonNull(userTicketValidator, "userTicketValidator must be non null");
	}

	@GetMapping("/tickets")
	public Collection<Ticket> getAllTickets() {
		return ticketManagementService.getAllTickets();
	}

	@PostMapping("/tickets/add")
	public void addTicket(@RequestBody @Valid TicketDto ticketDto, BindingResult result) {
		ticketValidator.validate(ticketDto, result);
		userTicketValidator.validate(ticketDto, result);
		if (!result.hasErrors()) {
			ticketManagementService.addTicket(ticketDto);
		} else {
			System.err.println(result.getAllErrors());
		}
	}

}
