package ro.gss.coredatabase.rest;

import java.util.Collection;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ro.gss.coredatabase.dto.UserRoleDto;
import ro.gss.coredatabase.entity.UserRole;
import ro.gss.coredatabase.service.UserRoleManagementService;

@RestController
@RequestMapping("/api")
public class UserRoleManagementController {

	private final UserRoleManagementService userRoleManagementService;

	@Autowired
	public UserRoleManagementController(final UserRoleManagementService userRoleManagementService) {
		this.userRoleManagementService = Objects.requireNonNull(userRoleManagementService,
				"roleManagementService must be non null");
	}

	@GetMapping("/userroles")
	public Collection<UserRole> getAllUserRoles() {
		return userRoleManagementService.getAllUserRoles();
	}

	@PostMapping("/userroles/add")
	public Long addUserRole(@RequestBody UserRoleDto userRoleDto) {
		return userRoleManagementService.addUserRole(userRoleDto);
	}

}
