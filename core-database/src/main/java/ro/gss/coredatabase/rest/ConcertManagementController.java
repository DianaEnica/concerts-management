package ro.gss.coredatabase.rest;

import java.util.Collection;
import java.util.Objects;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ro.gss.coredatabase.dto.ConcertDto;
import ro.gss.coredatabase.entity.Concert;
import ro.gss.coredatabase.service.ConcertManagementService;
import ro.gss.coredatabase.validation.ConcertValidator;

@RestController
@RequestMapping("/api")
public class ConcertManagementController {

	private final ConcertManagementService concertManagementService;
	private final ConcertValidator concertValidator;

	@Autowired
	public ConcertManagementController(ConcertManagementService concertManagementService,
			ConcertValidator concertValidator) {
		this.concertManagementService = Objects.requireNonNull(concertManagementService,
				"concertManagementService must be non null");
		this.concertValidator = Objects.requireNonNull(concertValidator, "concertValidator must be non null");
	}

	@GetMapping("/concerts")
	public Collection<Concert> getAllConcerts() {
		return concertManagementService.getAllConcerts();
	}

	@PostMapping("/concerts/addconcert")
	public void addConcert(@RequestBody @Valid ConcertDto concertDto, BindingResult result) {
		concertValidator.validate(concertDto, result);
     	if (!result.hasErrors()) {
			concertManagementService.addConcert(concertDto);
		} else {
			System.err.println(result.getAllErrors());
		}
	}

}
