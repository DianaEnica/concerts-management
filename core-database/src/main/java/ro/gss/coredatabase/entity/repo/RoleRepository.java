package ro.gss.coredatabase.entity.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import ro.gss.coredatabase.entity.Role;

public interface RoleRepository extends JpaRepository<Role, Long> {

}
