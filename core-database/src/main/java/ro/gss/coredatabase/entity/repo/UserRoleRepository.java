package ro.gss.coredatabase.entity.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import ro.gss.coredatabase.entity.UserRole;

public interface UserRoleRepository extends JpaRepository<UserRole, Long> {

}
