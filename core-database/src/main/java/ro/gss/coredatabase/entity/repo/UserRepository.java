package ro.gss.coredatabase.entity.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import ro.gss.coredatabase.entity.User;

public interface UserRepository extends JpaRepository<User, Long> {

	Long countByUsernameAndEmail(String username, String email);

}
