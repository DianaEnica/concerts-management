package ro.gss.coredatabase.entity.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import ro.gss.coredatabase.entity.Concert;

public interface ConcertRepository extends JpaRepository<Concert, Long> {

	Long countByOrganiserId(Long id);
}
