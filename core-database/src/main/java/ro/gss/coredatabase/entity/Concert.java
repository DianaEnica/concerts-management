package ro.gss.coredatabase.entity;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "concerts")
@Getter
@Setter
@NoArgsConstructor
public class Concert implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "seq_gen_concert", sequenceName = "seq_gen_concert", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_gen_concert")
	private Long id;

	private String name;

	@ManyToOne
	@JoinColumn(name = "id_organiser")
	User organiser;

	@Column(name = "concert_date")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date concertDate;

}
