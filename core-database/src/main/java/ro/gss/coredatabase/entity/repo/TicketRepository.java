package ro.gss.coredatabase.entity.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import ro.gss.coredatabase.entity.Ticket;

public interface TicketRepository extends JpaRepository<Ticket, Long> {

	Long countByConcertId(Long concertId);

	Long countByConcertIdAndUserId(Long concertId, Long userId);

}
