package ro.gss.coredatabase.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "tickets")
@Getter
@Setter
@NoArgsConstructor
public class Ticket implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "seq_gen_ticket", sequenceName = "seq_gen_ticket", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_gen_ticket")
	private Long id;

	@ManyToOne
	@JoinColumn(name = "concert_id")
	Concert concert;

	@ManyToOne
	@JoinColumn(name = "user_id")
	User user;

	private BigDecimal price;

	@Column(name = "purchased_date")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date purchaseDate;

}
