package ro.gss.coredatabase.dto;

import java.math.BigDecimal;

import javax.validation.constraints.NotNull;
//import javax.validation.constraints.Positive;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TicketDto {

	@NotNull
	private Long concertId;

	@NotNull
	private Long userId;

	@NotNull
	//@Positive
	private BigDecimal price;
	
}
