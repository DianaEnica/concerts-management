package ro.gss.coredatabase.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {

	@NotNull
	@Email(message = "Invalid email.")
	private String email;

	@NotNull
	@Pattern(regexp = "^[a-zA-Z][a-zA-Z0-9_]+")
	@Size(min = 5, max = 20, message = "Username should have 5-20 characters.")
	private String username;
	
	@NotNull
	@Pattern(regexp = "((?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{6,20})")
	private String password;

}
