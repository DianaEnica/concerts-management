package ro.gss.coredatabase.dto;


import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserRoleDto {

	@NotNull
	private Long userId;
	
	@NotNull
	private Long roleId;
	
}
