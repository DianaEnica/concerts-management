package ro.gss.coredatabase.dto;

import java.sql.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ConcertDto {

	@NotNull
	@Size(min = 2, max = 30)
	@Pattern(regexp = "[a-zA-Z]+")
	private String name;

	@NotNull
	private Long organiserId;

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date concertDate;

}
