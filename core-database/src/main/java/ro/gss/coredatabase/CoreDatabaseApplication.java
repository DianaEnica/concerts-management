package ro.gss.coredatabase;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CoreDatabaseApplication {

	public static void main(String[] args) {
		SpringApplication.run(CoreDatabaseApplication.class, args);
	}
}
