package ro.gss.coredatabase.service;

import java.util.Collection;
import java.util.Objects;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ro.gss.coredatabase.dto.UserRoleDto;
import ro.gss.coredatabase.entity.UserRole;
import ro.gss.coredatabase.entity.repo.RoleRepository;
import ro.gss.coredatabase.entity.repo.UserRepository;
import ro.gss.coredatabase.entity.repo.UserRoleRepository;

@Service
@Transactional
public class UserRoleManagementService {

	private final UserRoleRepository userRoleRepository;
	private final UserRepository userRepository;
	private final RoleRepository roleRepository;

	@Autowired
	public UserRoleManagementService(final UserRepository userRepository, final UserRoleRepository userRoleRepository,
			final RoleRepository roleRepository) {
		this.userRepository = Objects.requireNonNull(userRepository, "userRepository must be non null");
		this.userRoleRepository = Objects.requireNonNull(userRoleRepository, "userRoleRepository must be non null");
		this.roleRepository = Objects.requireNonNull(roleRepository, "roleRepository must be non null");
	}

	public Collection<UserRole> getAllUserRoles() {
		return userRoleRepository.findAll();
	}

	public Long addUserRole(UserRoleDto userRoleDto) {
		UserRole newUserRole = new UserRole();
		newUserRole.setUser(userRepository.getOne(userRoleDto.getUserId()));
		newUserRole.setRole(roleRepository.getOne(userRoleDto.getRoleId()));
		return userRoleRepository.save(newUserRole).getId();
	}

}
