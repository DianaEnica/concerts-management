package ro.gss.coredatabase.service;

import java.util.Collection;
import java.util.Objects;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ro.gss.coredatabase.dto.ConcertDto;
import ro.gss.coredatabase.entity.Concert;
import ro.gss.coredatabase.entity.repo.ConcertRepository;
import ro.gss.coredatabase.entity.repo.UserRepository;

@Service
@Transactional
public class ConcertManagementService {

	private ConcertRepository concertRepository;
	private UserRepository userRepository;

	@Autowired
	public ConcertManagementService(final ConcertRepository concertRepository, final UserRepository userRepository) {
		this.concertRepository = Objects.requireNonNull(concertRepository, "concertRepository must be non null");
		this.userRepository = Objects.requireNonNull(userRepository, "userRepository must be non null");
	}

	public Collection<Concert> getAllConcerts() {
		return concertRepository.findAll();
	}

	public void addConcert(ConcertDto concertDto) {
		Concert newConcert = new Concert();
		newConcert.setName(concertDto.getName());
		newConcert.setConcertDate(concertDto.getConcertDate());
		newConcert.setOrganiser(userRepository.getOne(concertDto.getOrganiserId()));
		concertRepository.save(newConcert);
	}

	public Long countByOrganiserId(Long id) {
		return concertRepository.countByOrganiserId(id);
	}
}
