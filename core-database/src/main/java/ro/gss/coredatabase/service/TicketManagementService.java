package ro.gss.coredatabase.service;

import java.util.Calendar;
import java.util.Collection;
import java.util.Objects;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ro.gss.coredatabase.dto.TicketDto;
import ro.gss.coredatabase.entity.Ticket;
import ro.gss.coredatabase.entity.repo.ConcertRepository;
import ro.gss.coredatabase.entity.repo.TicketRepository;
import ro.gss.coredatabase.entity.repo.UserRepository;

@Service
@Transactional
public class TicketManagementService {

	private TicketRepository ticketRepository;
	private ConcertRepository concertRepository;
	private UserRepository userRepository;

	@Autowired
	public TicketManagementService(final TicketRepository ticketRepository, final ConcertRepository concertRepository,
			final UserRepository userRepository) {
		this.ticketRepository = Objects.requireNonNull(ticketRepository, "ticketRepository must be non null");
		this.concertRepository = Objects.requireNonNull(concertRepository, "concertRepository must be non null");
		this.userRepository = Objects.requireNonNull(userRepository, "userRepository must be non null");
	}

	public Collection<Ticket> getAllTickets() {
		return ticketRepository.findAll();
	}

	public Long addTicket(TicketDto ticketDto) {
		Ticket newTicket = new Ticket();
		newTicket.setPrice(ticketDto.getPrice());
		newTicket.setPurchaseDate(Calendar.getInstance().getTime());
		newTicket.setConcert(concertRepository.getOne(ticketDto.getConcertId()));
		newTicket.setUser(userRepository.getOne(ticketDto.getUserId()));
		return ticketRepository.save(newTicket).getId();
	}

	public Long countByConcertId(Long concertId) {
		return ticketRepository.countByConcertId(concertId);
	}

	public Long countByConcertIdAndUserId(Long concertId, Long userId) {
		return ticketRepository.countByConcertIdAndUserId(concertId, userId);
	}

}
