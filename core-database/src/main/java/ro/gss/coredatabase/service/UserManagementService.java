package ro.gss.coredatabase.service;

import java.util.Calendar;
import java.util.Collection;
import java.util.Objects;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ro.gss.coredatabase.dto.UserDto;
import ro.gss.coredatabase.entity.User;
import ro.gss.coredatabase.entity.repo.UserRepository;

@Service
@Transactional
public class UserManagementService {

	private UserRepository userRepository;

	@Autowired
	public UserManagementService(final UserRepository userRepository) {
		this.userRepository = Objects.requireNonNull(userRepository, "userRepository must be non null");
	}

	public Collection<User> getAllUsers() {
		return userRepository.findAll();
	}

	public Long addUser(UserDto userdto) {
		User newUser = new User();
		newUser.setEmail(userdto.getEmail());
		newUser.setPassword(userdto.getPassword());
		newUser.setUsername(userdto.getUsername());
		newUser.setCreatedAt(Calendar.getInstance().getTime());
		return userRepository.save(newUser).getId();
	}

	public Long countByUsernameAndEmail(String username, String email) {
		return userRepository.countByUsernameAndEmail(username, email);
	}

}
