package ro.gss.coredatabase.validation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import ro.gss.coredatabase.dto.TicketDto;
import ro.gss.coredatabase.service.TicketManagementService;

@Component
public class UserTicketValidator implements Validator{

	@Autowired
	private TicketManagementService ticketManagementService;
	
	@Override
	public boolean supports(Class<?> clazz) {
		return TicketDto.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		Long concertId = ((TicketDto) target).getConcertId();
		Long userId = ((TicketDto)target).getUserId();
		if (ticketManagementService.countByConcertIdAndUserId(concertId, userId) > 3) {
			errors.reject("userId", "A user must have maxim 4 tickets per concert.");
		}
	}

}
