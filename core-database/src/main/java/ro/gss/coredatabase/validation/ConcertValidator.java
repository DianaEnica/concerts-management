package ro.gss.coredatabase.validation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import ro.gss.coredatabase.dto.ConcertDto;
import ro.gss.coredatabase.service.ConcertManagementService;

@Component
public class ConcertValidator implements Validator {

	@Autowired
	private ConcertManagementService concertManagementService;

	@Override
	public boolean supports(Class<?> clazz) {
		return ConcertDto.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		Long organiserId = ((ConcertDto) target).getOrganiserId();
		if (concertManagementService.countByOrganiserId(organiserId) > 0) {
			errors.reject("organiserId", "An organiser cannot deal with more than 1 concert");
		}
	}

}
