package ro.gss.coredatabase.validation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import ro.gss.coredatabase.dto.UserDto;
import ro.gss.coredatabase.service.UserManagementService;

@Component
public class UserValidator implements Validator {

	@Autowired
	private UserManagementService userManagementService;

	@Override
	public boolean supports(Class<?> clazz) {
		return UserDto.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		String username = ((UserDto) target).getUsername();
		String email = ((UserDto) target).getEmail();
		if (userManagementService.countByUsernameAndEmail(username, email) > 0) {
			errors.reject("username", "Username already taken.");
		}
	}

}
