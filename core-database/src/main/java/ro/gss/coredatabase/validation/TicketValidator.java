package ro.gss.coredatabase.validation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import ro.gss.coredatabase.dto.TicketDto;
import ro.gss.coredatabase.service.TicketManagementService;

@Component
public class TicketValidator implements Validator {

	@Autowired
	private TicketManagementService ticketManagementService;

	@Override
	public boolean supports(Class<?> clazz) {
		return TicketDto.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		Long concertId = ((TicketDto) target).getConcertId();
		if (ticketManagementService.countByConcertId(concertId) > 9) {
			errors.reject("concertId", "A concert must have maximum 10 tickets.");
		}
	}

}
