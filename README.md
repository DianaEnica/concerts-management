# Concerts Management System

### Overview

  * In this project, you can add/update/delete/show a concert, aticket, an user and an user-role for a certain user.   
  * Of course, any data that you will introduce, will be validated and then added to the database.
	
### Implementation Method

- The application itself has a MVC structure for separating the user interface from the implementation and database. 
- It is implemented in Spring. (Java Framework)  
- Apache Maven helped me build and manage the project with dependencies and with dynamic downloading Java libraries. (pom.xml)
- The graphic interface was made using a Swagger Configuration. It looks more intuitive and easy to use.  
- The server runs with a Tomcat server.
- I've used a relational database in PostgreSQL and I added also a schema for better understanding. 
- For a user graphic interface of the database and for creating the initial schema "core-database" I've used PgAdmin4.

### Requirements

Spring Tool Suite, JDK 1.8, JDBC, JPA, Tomcat, Maven PostgreSQL, PgAdmin4.

© Enica Diana - Maria 2018